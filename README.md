## Can't connect to kernel

See the [IPython docs](https://ipython.readthedocs.io/en/stable/install/kernel_install.html#kernels-for-different-environments)
Every kernel needs a unique name and whatever.

`env/bin/python -m ipykernel install --user --name env --display-name "Python (Jupyter env)"`
